//
//  CustomAlertNode.swift
//  SpaceRace
//
//  Created by Hariharan S on 15/06/24.
//

import SpriteKit

class CustomAlertNode: SKNode {
    init(size: CGSize, title: String, message: String) {
        super.init()

        // Background
        let background = SKShapeNode(rectOf: size, cornerRadius: 10)
        background.fillColor = .gray
        background.zPosition = 100
        self.addChild(background)
        
        // Title Label
        let titleLabel = SKLabelNode(text: title)
        titleLabel.fontName = "Helvetica-Bold"
        titleLabel.fontSize = 20
        titleLabel.fontColor = .white
        titleLabel.position = CGPoint(x: 0, y: size.height / 4)
        titleLabel.zPosition = 101
        self.addChild(titleLabel)

        // Message Label
        let messageLabel = SKLabelNode(text: message)
        messageLabel.fontName = "Helvetica"
        messageLabel.fontSize = 16
        messageLabel.fontColor = .white
        messageLabel.position = CGPoint(x: 0, y: 0)
        messageLabel.zPosition = 101
        self.addChild(messageLabel)

        // OK Button
        let okButton = SKLabelNode(text: "OK")
        okButton.name = "okButton"
        okButton.fontName = "Helvetica-Bold"
        okButton.fontSize = 18
        okButton.fontColor = .blue
        okButton.position = CGPoint(x: 0, y: -size.height / 4)
        okButton.zPosition = 101
        self.addChild(okButton)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

