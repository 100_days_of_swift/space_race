//
//  GameScene.swift
//  SpaceRace
//
//  Created by Hariharan S on 12/06/24.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    var starfield: SKEmitterNode!
    var player: SKSpriteNode!

    var scoreLabel: SKLabelNode!
    var score = 0 {
        didSet {
            self.scoreLabel.text = "Score: \(self.score)"
        }
    }
    
    let possibleEnemies = ["ball", "hammer", "tv"]
    var isGameOver = false
    var gameTimer: Timer?
    
    override func touchesMoved(
        _ touches: Set<UITouch>,
        with event: UIEvent?
    ) {
        guard let touch = touches.first
        else {
            return
        }
        var location = touch.location(in: self)

        if location.y < 100 {
            location.y = 100
        } else if location.y > 668 {
            location.y = 668
        }
        self.player.position = location
    }
    
    override func touchesBegan(
        _ touches: Set<UITouch>,
        with event: UIEvent?
    ) {
        for touch in touches {
            let location = touch.location(in: self)
            let touchedNodes = nodes(at: location)
            for node in touchedNodes {
                if node.name == "okButton" {
                    node.parent?.removeFromParent()
                }
            }
        }
    }
    
    override func touchesEnded(
        _ touches: Set<UITouch>,
        with event: UIEvent?
    ) {
        self.isGameOver = true
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let explosion = SKEmitterNode(fileNamed: "explosion")!
        explosion.position = self.player.position
        self.addChild(explosion)

        self.player.removeFromParent()
        self.isGameOver = true
        self.showCustomAlert(
            title: "Game Over !!!",
            message: "Your Score is \(self.score)"
        )
    }
    
    override func didMove(to view: SKView) {
        self.backgroundColor = .black

        self.starfield = SKEmitterNode(fileNamed: "starfield")!
        self.starfield.position = CGPoint(
            x: 1024,
            y: 384
        )
        self.starfield.advanceSimulationTime(10)
        self.addChild(self.starfield)
        self.starfield.zPosition = -1

        self.player = SKSpriteNode(imageNamed: "player")
        self.player.position = CGPoint(
            x: 100,
            y: 384
        )
        self.player.physicsBody = SKPhysicsBody(
            texture: self.player.texture!,
            size: self.player.size
        )
        self.player.physicsBody?.contactTestBitMask = 1
        self.addChild(self.player)

        self.scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        self.scoreLabel.position = CGPoint(
            x: 16,
            y: 16
        )
        self.scoreLabel.horizontalAlignmentMode = .left
        self.addChild(self.scoreLabel)
        self.score = 0

        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        self.physicsWorld.contactDelegate = self
        
        self.gameTimer = Timer.scheduledTimer(
            timeInterval: 0.35,
            target: self,
            selector: #selector(self.createEnemy),
            userInfo: nil,
            repeats: true
        )
    }
    
    override func update(_ currentTime: TimeInterval) {
        for node in self.children {
            if node.position.x < -300 {
                node.removeFromParent()
            }
        }
        
        if !self.isGameOver {
            self.score += 1
        }
    }
    
    func showCustomAlert(
        title: String,
        message: String
    ) {
        let alertSize = CGSize(width: 300, height: 200)
        let alert = CustomAlertNode(size: alertSize, title: title, message: message)
        alert.position = CGPoint(x: size.width / 2, y: size.height / 2)
        self.addChild(alert)
    }
    
    @objc func createEnemy() {
        guard let enemy = self.possibleEnemies.randomElement(),
              !self.isGameOver
        else {
            return
        }

        let sprite = SKSpriteNode(imageNamed: enemy)
        sprite.position = CGPoint(
            x: 1200,
            y: Int.random(in: 50...736)
        )
        self.addChild(sprite)

        sprite.physicsBody = SKPhysicsBody(
            texture: sprite.texture!,
            size: sprite.size
        )
        sprite.physicsBody?.categoryBitMask = 1
        sprite.physicsBody?.velocity = CGVector(dx: -500, dy: 0)
        sprite.physicsBody?.angularVelocity = 5
        sprite.physicsBody?.linearDamping = 0
        sprite.physicsBody?.angularDamping = 0
    }
}
